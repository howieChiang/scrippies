#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-group-projects

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

cleanup() {
    status="$?"
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... GROUP
Get the list of GROUP projects.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 realtime-robotics

EOF
}

spit_vars() {
    for var; do
        info "$var:" "$(
            set -o noglob
            eval echo "\$$var"
        )"
    done
}

posix_namify() {
    printf '%s' "$*" | tr -sc '[:alnum:]._-' '_'
}

curlxxx() {
    curl \
        --silent \
        --fail \
        --retry 3 \
        --show-error \
        --location \
        --header "${HEADER}" \
        "$@"
}

curlget() {
    info "getting..."
    curlxxx --request GET "$@"
}

curlgetlist() (
    # temp files to track state
    result="$(mktemp -t result.XXXXXX)"
    output="$(mktemp -ut output.XXXXXX)"
    dump_header="$(mktemp -ut dump_header.XXXXXX)"

    # indices and counters
    page="0"
    per_page="50"
    total="1"

    # loop
    while [ "$((page * per_page))" -lt "${total}" ]; do
        page="$((page + 1))"
        info "page:" "${page}"
        curlget \
            --data-urlencode "per_page=${per_page}" \
            --data-urlencode "page=${page}" \
            --dump-header "${dump_header}" \
            --output "${output}" \
            "$@"

        # the response header has DOS-style CRLF line endings
        sed -i 's/\r//g' "${dump_header}"
        if ! total="$(grep -Ex 'x-total: [[:digit:]]+' "${dump_header}" | grep -Eo '[[:digit:]]+$')"; then
            error "failed to parse x-total from response header:"
            cat -n "${dump_header}" >&2
            return 1
        fi

        # https://stackoverflow.com/questions/60099083/use-jq-to-concatenate-json-arrays-in-multiple-files/60099456#60099456
        jq -n 'reduce inputs[] as $d (.; . += [$d])' "${result}" "${output}" >"${result}.new"
        mv "${result}.new" "${result}"
    done

    # spit to stdout
    jq . "${result}"

    # clean up temp files
    rm -f "${dump_header}" "${output}" "${result}"
)

# $1 : path
prettify() {
    info "prettifying..."
    jq . "$1" >"$1".pretty
    mv "$1".pretty "$1"
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if ! [ 1 -eq "$#" ]; then
    die "bad args"
fi

GROUP="$1"

################################################################################

if [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    HEADER="PRIVATE-TOKEN: ${GITLAB_API_PRIVATE_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    HEADER="JOB-TOKEN: ${CI_JOB_TOKEN}"
else
    die "need one, missing all:" "GITLAB_API_PRIVATE_TOKEN" "CI_JOB_TOKEN"
fi

################################################################################

export GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"
spit_vars GITLAB_API_ENDPOINT

################################################################################

GROUP_PROJECTS_URL="$(
    printf \
        '%s/groups/%s/projects' \
        "${GITLAB_API_ENDPOINT}" \
        "${GROUP}"
)"
GROUP_PROJECTS_OUT="$(posix_namify "${GROUP_PROJECTS_URL}".json)"
spit_vars GROUP_PROJECTS_URL GROUP_PROJECTS_OUT

curlgetlist "${GROUP_PROJECTS_URL}" >"${GROUP_PROJECTS_OUT}"

info "done"

exit "$?"
