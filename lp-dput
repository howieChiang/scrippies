#!/bin/sh
# shellcheck disable=SC2317
# scrippies/lp-dput

set -euvx

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... SOURCE_NAME
Publish to launchpad the given source package.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 ros-foxy-ament-cmake-nose

EOF
}

spit_vars() {
    for var; do
        info "$var: $(eval echo "\$$var")"
    done
}

find_file_regex() {
    find \
        "${PWD}" \
        -maxdepth 1 \
        -mindepth 1 \
        -type f \
        -regextype posix-extended \
        -regex "$1" \
        -print0 | xargs -0n1 realpath
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if ! [ 1 -eq "$#" ]; then
    die "bad args"
fi
SOURCE_NAME="$1"

if ! [ -x "${here}/dpkg-buildpackage-dput" ]; then
    die "missing prog:" "${here}/dpkg-buildpackage-dput"
fi

# version regular expression
VRE='[[:alnum:].+~-]+'

if SOURCE_CHANGES_FILE="$(
    find_file_regex "${PWD}/${SOURCE_NAME}_${VRE}_source[.]changes"
)"; then
    die "extant file:" "${SOURCE_CHANGES_FILE}"
fi

apt-get source "${SOURCE_NAME}"
if ! DSC_FILE="$(
    find_file_regex "${PWD}/${SOURCE_NAME}_${VRE}[.]dsc"
)"; then
    die "failed to find DSC_FILE"
fi
spit_vars DSC_FILE
cat -n "${DSC_FILE}" >&2

if ! ORIGTAR_BASENAME="$(
    grep \
        -Eom1 \
        "${SOURCE_NAME}_${VRE}[.]orig[.]tar([.](gz|bz2|lzma|xz))?$" \
        "${DSC_FILE}"
)"; then
    die "failed to discover: ORIGTAR_BASENAME"
fi
spit_vars ORIGTAR_BASENAME

if ! ORIGTAR="$(realpath "${PWD}/${ORIGTAR_BASENAME}")"; then
    die "missing path:" "${PWD}/${ORIGTAR_BASENAME}"
fi
if ! [ -f "${ORIGTAR}" ]; then
    die "missing file:" "${ORIGTAR}"
fi
spit_vars ORIGTAR

if ! ORIGDIR_BASENAME="$(
    tar -tf "${ORIGTAR}" \
        | grep -Eom1 "^${SOURCE_NAME}[^/]+"
)"; then
    die "failed to discover: ORIGDIR_BASENAME"
fi
spit_vars ORIGDIR_BASENAME

if ! ORIGDIR="$(realpath "${PWD}/${ORIGDIR_BASENAME}")"; then
    die "missing path:" "${PWD}/${ORIGDIR_BASENAME}"
fi
if ! [ -d "${ORIGDIR}" ]; then
    die "missing dir:" "${ORIGDIR}"
fi
spit_vars ORIGDIR

(
    cd "${ORIGDIR}"
    eval "${here}/dpkg-buildpackage-dput"
)

exit "$?"
