#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-pipeline-schedules

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

cleanup() {
    status="$?"
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... PROJECT_URL
For the given repository, download the pipeline schedules and last pipelines.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 https://gitlab.com/realtime-robotics/rapidplan

EOF
}

urlencode() {
    jq -sRr @uri
}

urldecode() {
    sed -E -e 's/[+]/ /g' -e 's/[%]([[:xdigit:]]{2})/\\\\x\1/g' \
        | xargs printf '%b\n'
}

# read stdin, write POSIX-safe basename
posix_namify() {
    # https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_282
    # "Portable Filename Character Set"
    sed -E 's/[^[:alnum:].-]+/_/g;s/^_//;s/_$//'
}

# $1 : url
url2out() {
    printf '%s.json' "$1" \
        | urldecode \
        | posix_namify
}

# $1 : url
# $2 : file
curl_url_out() {
    info getting: "$1"
    info writing: "$2"
    curl \
        --fail \
        --retry 3 \
        --show-error \
        --silent \
        --location \
        --request GET \
        --header "${HEADER}" \
        --output "$2" \
        "$1"
    jq 'if type=="array" then sort else . end' "$2" >"$2".pretty
    mv "$2".pretty "$2"
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if ! command -v curl >/dev/null 2>&1; then
    die missing command: curl
fi

if ! command -v jq >/dev/null 2>&1; then
    die missing command: jq
fi

################################################################################

if [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    HEADER="PRIVATE-TOKEN: ${GITLAB_API_PRIVATE_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    HEADER="JOB-TOKEN: ${CI_JOB_TOKEN}"
else
    die "need one, missing all:" "GITLAB_API_PRIVATE_TOKEN" "CI_JOB_TOKEN"
fi

################################################################################

# Allow the caller to override this.
GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"
info GITLAB_API_ENDPOINT: "${GITLAB_API_ENDPOINT}"

################################################################################

# vet the given CI_PROJECT_URL
if ! [ 1 -eq "$#" ]; then
    die bad args
fi
if ! CI_PROJECT_URL="$(
    echo "$1" \
        | grep -Ex 'https://gitlab[.]com/([^/]+(/[^/]+)*)'
)"; then
    die bad CI_PROJECT_URL: "$1"
fi

# Parse the CI_PROJECT_URL to get the CI_PROJECT_ID; this is an url-encoded
# string of path components following 'https://gitlab.com/'.
#
# https://docs.gitlab.com/ee/api/index.html#namespaced-path-encoding
if ! CI_PROJECT_ID="$(
    printf '%s' "${CI_PROJECT_URL}" \
        | sed -E 's|https://gitlab[.]com/([^/]+(/[^/]+)*)|\1|' \
        | urlencode
)"; then
    die failed to parse CI_PROJECT_ID from "${CI_PROJECT_URL}"
fi
info CI_PROJECT_ID: "${CI_PROJECT_ID}"

################################################################################

PIPELINE_SCHEDULES_URL="$(
    printf \
        '%s/projects/%s/pipeline_schedules' \
        "${GITLAB_API_ENDPOINT}" \
        "${CI_PROJECT_ID}"
)"
#info PIPELINE_SCHEDULES_URL: "${PIPELINE_SCHEDULES_URL}"
PIPELINE_SCHEDULES_OUT="$(url2out "${PIPELINE_SCHEDULES_URL}")"
#info PIPELINE_SCHEDULES_OUT: "${PIPELINE_SCHEDULES_OUT}"
curl_url_out \
    "${PIPELINE_SCHEDULES_URL}" \
    "${PIPELINE_SCHEDULES_OUT}"

################################################################################

for id in $(jq '.[].id' "${PIPELINE_SCHEDULES_OUT}"); do
    PIPELINE_SCHEDULE_URL="${PIPELINE_SCHEDULES_URL}"/"${id}"
    #info PIPELINE_SCHEDULE_URL: "${PIPELINE_SCHEDULE_URL}"
    PIPELINE_SCHEDULE_OUT="$(url2out "${PIPELINE_SCHEDULE_URL}")"
    #info PIPELINE_SCHEDULE_OUT: "${PIPELINE_SCHEDULE_OUT}"
    curl_url_out \
        "${PIPELINE_SCHEDULE_URL}" \
        "${PIPELINE_SCHEDULE_OUT}"

    LAST_PIPELINE_ID="$(jq .last_pipeline.id "${PIPELINE_SCHEDULE_OUT}")"
    #info LAST_PIPELINE_ID: "${LAST_PIPELINE_ID}"

    PIPELINE_JOBS_URL="$(
        printf \
            '%s/projects/%s/pipelines/%s/jobs' \
            "${GITLAB_API_ENDPOINT}" \
            "${CI_PROJECT_ID}" \
            "${LAST_PIPELINE_ID}"
    )"
    #info PIPELINE_JOBS_URL: "${PIPELINE_JOBS_URL}"
    PIPELINE_JOBS_OUT="$(url2out "${PIPELINE_JOBS_URL}")"
    #info PIPELINE_JOBS_OUT: "${PIPELINE_JOBS_OUT}"
    curl_url_out \
        "${PIPELINE_JOBS_URL}" \
        "${PIPELINE_JOBS_OUT}"
done

exit "$?"

# References:
#
# * https://docs.gitlab.com/ee/api/pipeline_schedules.html
