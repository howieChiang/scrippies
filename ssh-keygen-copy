#!/bin/sh
# shellcheck disable=SC2317
# scrippies/ssh-keygen-copy

set -eu

export LC_ALL=C

this="$(realpath -e "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" | while read -r path; do
        chmod 1777 "${path}"
        echo "${path}"
    done
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

usage() {
    cat <<EOF

Usage: $0 [OPTION]... [USER@]HOST
For the given [USER@]HOST, ssh-keyscan, ssh-keygen, and ssh-copy-id.

Options:

    -h    print this usage and return success
    -L    local changes only: stop before ssh-copy-id

Examples:

    \$ $0 -h

    \$ $0 luser@host42.example.com

Notes:

  * requirements

    * \`sshpass\` command

    * defined non-empty \$SSHPASS environment variable

  * creates (never overwrites) the following files:

    * \$HOME/.ssh/kh_\$1     : ssh known hosts file

    * \$HOME/.ssh/id_\$1     : ssh private key

    * \$HOME/.ssh/id_\$1.pub : ssh public key

  * works well with the following ssh config snippet:

    Host \${1#*@}
    User \${1%@*}
    IdentityFile ~/.ssh/id_\$1
    UserKnownHostsFile ~/.ssh/kh_\$1

EOF
}

noisy_eval() (
    info "RUNNING:" "$@"
    rc=""
    if "$@"; then
        rc="$?"
        info "SUCCESS:" "$@"
    else
        rc="$?"
        error "FAILURE:" "$@"
    fi
    return "${rc}"
)

################################################################################
################################################################################
################################################################################

while getopts ":hL" opt; do
    case "${opt}" in
        h)
            usage
            exit "$?"
            ;;
        L) readonly requested_local="true" ;;
        :) die "Missing argument: -${OPTARG}" ;;
        \?) die "Invalid option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

export SSH_OPTS="-F /dev/null"

if ! command -v sshpass >/dev/null 2>&1; then
    die "missing command: sshpass"
fi

if ! [ 1 -eq "$#" ]; then
    die "bad args"
fi

# https://stackoverflow.com/questions/106179/regular-expression-to-match-dns-hostname-or-ip-address
#
# hname = name(.name)*
# name = [[:alnum:]]([[:alnum:]-]*[[:alnum:]])*
# ^[[:alnum:]]([[:alnum:]-]*[[:alnum:]])*([.][[:alnum:]]([[:alnum:]-]*[[:alnum:]])*)*$
if ! echo "$1" | grep -Eq '([a-z_][a-z0-9_-]*[$]?[@])?[a-z0-9]([a-z0-9-]*[a-z0-9])*([.][a-z0-9]([a-z0-9-]*[a-z0-9])*)*$'; then
    die "bad argument: $1"
fi

readonly khosts="${HOME}/.ssh/kh_${1}"
readonly prikey="${HOME}/.ssh/id_${1}"
readonly pubkey="${HOME}/.ssh/id_${1}.pub"
readonly rhostname="${1#*@}"

################################################################################

# known hosts
if [ -f "${khosts}" ]; then
    warning "extant known hosts: ${khosts}"
else
    info "creating known hosts: ${khosts}"
    noisy_eval ssh-keyscan "${rhostname}" >"${khosts}"
fi

noisy_eval chmod -v 0600 "${khosts}" >&2
noisy_eval ssh-keygen -F "${rhostname}" -f "${khosts}" >&2

################################################################################

# private key
if [ -f "${prikey}" ]; then
    warning "extant private key: ${prikey}"
else
    info "creating private key: ${prikey}"
    noisy_eval ssh-keygen \
        -vvv \
        -N '' \
        -C "$(whoami)@$(hostname):~${prikey#"${HOME}"}" \
        -f "${prikey}"
fi

noisy_eval chmod -v 0400 "${prikey}" >&2
noisy_eval ssh-keygen -l -f "${prikey}" >&2

################################################################################

# public key

if ! [ -f "${pubkey}" ]; then
    warning "missing public key:" "${pubkey}"
    warning "generating public key from private key"
    noisy_eval ssh-keygen -vvv -y -f "${prikey}" >"${pubkey}"
fi

noisy_eval chmod -v 0644 "${pubkey}" >&2
noisy_eval ssh-keygen -l -f "${pubkey}" >&2

################################################################################

# stop if requested local

if [ "true" = "${requested_local:-false}" ]; then
    exit 0
fi

################################################################################

# ssh-copy-id

if [ -z "${SSHPASS+x}" ]; then
    die "undefined/empty env var: SSHPASS"
fi

noisy_eval sshpass -e ssh-copy-id \
    -i "${prikey}" \
    -o PreferredAuthentications=password \
    -o PubkeyAuthentication=no \
    -o StrictHostKeyChecking=yes \
    -o UserKnownHostsFile="${khosts}" \
    "$1"

noisy_eval ssh \
    -i "${prikey}" \
    -o PreferredAuthentications=publickey \
    -o PubkeyAuthentication=yes \
    -o StrictHostKeyChecking=yes \
    -o UserKnownHostsFile="${khosts}" \
    "$1" \
    'uname -a && date -uIseconds'

exit "$?"

####################
# man 1 ssh-keygen #
####################
#
# https://manpages.debian.org/bullseye/openssh-client/ssh-keygen.1.en.html
#
#
# ssh-keygen -F hostname [-lv] [-f known_hosts_file]
#
# -F hostname | [hostname]:port
#
# Search for the specified hostname (with optional port number) in a
# known_hosts file, listing any occurrences found. This option is useful to
# find hashed host names or addresses and may also be used in conjunction with
# the -H option to print found keys in a hashed format.
#
#
# ssh-keygen -R hostname [-f known_hosts_file]
#
# -R hostname | [hostname]:port
#
# Removes all keys belonging to the specified hostname (with optional port
# number) from a known_hosts file. This option is useful to delete hashed hosts
# (see the -H option above).
#
#
# ssh-keygen -H [-f known_hosts_file]
#
# -H
#
# Hash a known_hosts file. This replaces all hostnames and addresses with
# hashed representations within the specified file; the original content is
# moved to a file with a .old suffix. These hashes may be used normally by ssh
# and sshd, but they do not reveal identifying information should the file's
# contents be disclosed. This option will not modify existing hashed hostnames
# and is therefore safe to use on files that mix hashed and non-hashed names.
#
#
# -f filename
#
# Specifies the filename of the key file.
#
#
# -l
#
# Show fingerprint of specified public key file. For RSA and DSA keys
# ssh-keygen tries to find the matching public key file and prints its
# fingerprint. If combined with -v, a visual ASCII art representation of the
# key is supplied with the fingerprint.
#
# -v
#
# Verbose mode. Causes ssh-keygen to print debugging messages about its
# progress. This is helpful for debugging mod- uli generation. Multiple -v
# options increase the verbosity. The maximum is 3.
#
