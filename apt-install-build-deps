#!/bin/sh
# shellcheck disable=SC2317
# scrippies/apt-install-build-deps

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [MK_BUILD_DEPS_ARG...] [-- APT_ARG...]
Install build dependencies.

Examples:

    \$ $0

    \$ $0 -- --yes --simulate --reinstall

    \$ $0 debian/control

    \$ $0 debian/control -- --yes --simulate --reinstall

    \$ $0 tree

    \$ $0 tree -- --yes --simulate --reinstall

Notes:

    * If no MK_BUILD_DEPS_ARG... are given, then default to 'debian/control' if
      it exists.

EOF
}

noisy_eval() {
    info "RUNNING:" "$@"
    if "$@"; then
        rc="$?"
        info "SUCCESS:" "$@"
    else
        rc="$?"
        error "FAILURE:" "$@"
    fi
    return "${rc}"
}

# https://www.etalabs.net/sh_tricks.html
# Rich's sh (POSIX shell) tricks
# Working with arrays
save_args() {
    for i; do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"; done
    echo " "
}

################################################################################
################################################################################
################################################################################

for arg in "$@"; do
    shift
    if [ "${arg}" = "--" ]; then
        break
    fi
    mk_build_deps_args="$(
        eval "set -- ${mk_build_deps_args-}"
        save_args "$@" "${arg}"
    )"
done

# This is how mk-build-deps treats positional arguments. Imitate those
# semantics.
if [ -z "${mk_build_deps_args-}" ]; then
    if [ -r "${PWD}"/debian/control ]; then
        mk_build_deps_args="${PWD}"/debian/control
    fi
fi
if [ -z "${mk_build_deps_args-}" ]; then
    die bad mk_build_deps_args
fi

apt_args="$(save_args "$@")"

(
    eval "set -- ${mk_build_deps_args-}"
    info mk_build_deps_args: "$@"
    eval "set -- ${apt_args-}"
    info apt_args: "$@"
)

################################################################################

(
    # Descend into a subshell and change into a temporary directory. Do this
    # because it simplifies finding the '*.deb' file(s) we will create.
    cd "$(mktemp -dt repodir.XXXXXX)"

    ################
    # UPSTREAM BUG #
    ################
    #
    # The `equivs-build` script shipping with the `equivs` package version
    # 2.1.0 does weird things when TMPDIR is set.
    #
    # "equivs builds packages in surprising location"
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=897927
    #
    # "devscripts: mk-build-deps creates the build-dep package in the wrong
    # directory"
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=875677
    #
    # "please add option for tempdir location"
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=660765
    (
        unset TMPDIR
        eval "set -- ${mk_build_deps_args-}"
        noisy_eval mk-build-deps "$@"
    )

    if ! find "${PWD}" -type f -name '*.deb' | grep -q .; then
        die missing '*.deb' files: "${PWD}"
    fi

    (
        eval "set -- ${apt_args-}"
        find "${PWD}" -type f -name '*.deb' | while read -r deb; do
            noisy_eval "${here}"/install-deb "${deb}" "$@"
        done
    )
)

exit "$?"
