#!/bin/sh
# shellcheck disable=SC2317
# scrippies/crongen

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}"'['"$$"']:' "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

default_cron_template='%u %u * * *'

usage() {
    cat <<EOF

Usage: $0 [OPTION]... [CRON_TEMPLATE [RAND_SEED]...]
Generate a cron expression, maybe with random numbers.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0

    \$ $0 '%u %u * * %u' my stuff every week

    \$ $0 '%u %u * * *' something every day

    \$ $0 '%u %u * * %u' another thing every week

    \$ $0 '%u %u %u * *' yet another thing every month

    \$ $0 '%u %u * 2 *' daily during black history month

    \$ $0 '%u %u * 6 *' daily during pride month

Notes:

    * CRON_TEMPLATE defaults to '${default_cron_template}'.

    * Any '%u' field expands to a random number within the proper range.

    * All arguments following the first are used as RAND_SEED strings.

EOF
}

# Take a positive integer. If it is a prime number, then return success;
# otherwise return failure.
is_prime() (
    for d in $(seq 2 "$(($1 / 2))"); do
        if [ 0 -eq "$(($1 % d))" ]; then
            return 1
        fi
    done
    return 0
)

# Find and print the smallest prime number strictly greater than the given
# positive integer.
next_prime() (
    result="$(($1 + 1))"
    while ! is_prime "${result}"; do
        result="$((result + 1))"
    done
    echo "${result}"
)

# Compute and print the CRC32 digest of the 'SEED'.
seedsum() (
    printf '%s' "${SEED}" \
        | cksum \
        | grep -Eom1 '^[[:digit:]]+'
)

# Take a positive integer modulus, 'm'. From the set of integers in the range
# [0,m-1], print a member chosen "randomly" on the basis of the 'SEED'.
randn0() (
    printf '%u' "$(($(seedsum) % $(next_prime "$1") % $1))"
)

# Take a positive integer modulus, 'm'. From the set of integers in the range
# [1,m], print a member chosen "randomly" on the basis of the 'SEED'.
randz1() (
    printf '%u' "$(($(randn0 "$1") + 1))"
)

################################################################################
################################################################################
################################################################################

# Parse options.
while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

case "$#" in
    0)
        readonly cron_template='%u %u * * *'
        info defaulting cron_template: "${cron_template}"
        ;;
    *)
        readonly cron_template="$1"
        warning overriding cron_template: "${cron_template}"
        shift
        ;;
esac
SEED="$*"

cron_template_min="$(echo "${cron_template}" | tr -s ' ' ' ' | cut -d' ' -f1)"
cron_template_hou="$(echo "${cron_template}" | tr -s ' ' ' ' | cut -d' ' -f2)"
cron_template_dom="$(echo "${cron_template}" | tr -s ' ' ' ' | cut -d' ' -f3)"
cron_template_moy="$(echo "${cron_template}" | tr -s ' ' ' ' | cut -d' ' -f4)"
cron_template_dow="$(echo "${cron_template}" | tr -s ' ' ' ' | cut -d' ' -f5)"
#info cron_template_min: "${cron_template_min}"
#info cron_template_hou: "${cron_template_hou}"
#info cron_template_dom: "${cron_template_dom}"
#info cron_template_moy: "${cron_template_moy}"
#info cron_template_dow: "${cron_template_dow}"

################################################################################

# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/crontab.html

# cron_expression_min: [0,60]
if [ '%u' = "${cron_template_min}" ]; then
    cron_expression_min="$(randn0 60)"
else
    cron_expression_min="${cron_template_min}"
fi
#info cron_expression_min: "${cron_expression_min}"

# cron_expression_hou: [0,23]
if [ '%u' = "${cron_template_hou}" ]; then
    cron_expression_hou="$(randn0 24)"
else
    cron_expression_hou="${cron_template_hou}"
fi
#info cron_expression_hou: "${cron_expression_hou}"

# cron_expression_dom: [1,31]
if [ '%u' = "${cron_template_dom}" ]; then
    cron_expression_dom="$(randz1 31)"
else
    cron_expression_dom="${cron_template_dom}"
fi
#info cron_expression_dom: "${cron_expression_dom}"

# cron_expression_moy: [1,12]
if [ '%u' = "${cron_template_moy}" ]; then
    cron_expression_moy="$(randz1 12)"
else
    cron_expression_moy="${cron_template_moy}"
fi
#info cron_expression_moy: "${cron_expression_moy}"

# cron_expression_dow: [0,6]
if [ '%u' = "${cron_template_dow}" ]; then
    cron_expression_dow="$(randn0 7)"
else
    cron_expression_dow="${cron_template_dow}"
fi
#info cron_expression_dow: "${cron_expression_dow}"

################################################################################

# Construct the cron expression.
cron_expression="$(
    printf \
        '%2s %2s %2s %2s %2s' \
        "${cron_expression_min}" \
        "${cron_expression_hou}" \
        "${cron_expression_dom}" \
        "${cron_expression_moy}" \
        "${cron_expression_dow}"
)"

# Does this 'crontab' support the '-n' "dry run" flag? Yes, a zero-length file
# is a valid crontab.
if crontab -n - </dev/null >/dev/null 2>&1; then
    # Validate the cron expression.
    if ! echo "${cron_expression}" | crontab -n -; then
        die syntax error: "${cron_expression}"
    fi
else
    warning unsupported: crontab -n
    warning cannot validate cron expression
fi

echo "${cron_expression}"
