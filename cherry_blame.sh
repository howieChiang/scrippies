#!/bin/sh
# shellcheck disable=SC2317
usage() {
    cat <<EOF

Usage: $0 [OPTION]... BASE_BRANCH TARGET_BRANCH
Finds commits in BASE_BRANCH that are missing from TARGET_BRANCH

Options:

    -h            print help and exit
    -C WORKDIR    run as if started in WORKDIR (default: \$PWD)

Examples:

    \$ $0 release-1.4.2 master

    \$ $0 -C \$PWD release-1.4.2 master

EOF
}

die() {
    error "$@"
    usage >&2
    exit 1
}

while getopts ":hC:" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        C) readonly WORKDIR="${OPTARG}" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if ! [ 2 -eq "$#" ]; then
    echo "2 inputs only"
    exit 1
fi

readonly BASE_BRANCH="$1"
readonly TARGET_BRANCH="$2"

GIT_LOG=$(git -C "$WORKDIR" log "$TARGET_BRANCH")

# use git cherry to identify commit diff. + prefixed commits indicate it is a commit missing from
# TARGET branch
commits=$(git -C "$WORKDIR" cherry -v "$TARGET_BRANCH" "$BASE_BRANCH" | grep '^+' | awk '{print $2}')
for c in $commits; do
    # original commit title
    commit_title=$(git -C "$WORKDIR" show -s --format='%s' "$c")

    # add escape to special characters
    fixed_title=$(echo "$commit_title" | sed 's/[^a-zA-Z0-9 ()-+_/@=:.%,`<>?]/\\&/g')

    # check if grep can find the commit title in the git log for TARGET branch
    if ! echo "$GIT_LOG" | grep -q -- "$fixed_title"; then
        # show commit info to terminal
        git -C "$WORKDIR" show -s --format='%h,%as,%s,%ae,%aN' "$c"
    fi
done
