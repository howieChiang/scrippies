#!/usr/bin/make -f

# scrippies/repolize.mk

.PHONY: all
all: apt-config/etc/apt/apt.conf debootstrap/minbase.status

.PHONY: test
test:
	@echo $@

.PHONY: clean
clean:
	@$(RM) -v -r debootstrap apt-config apt-config.mk

################################################################################

debs := $(sort $(wildcard *.deb))

################################################################################

debootstrap/minbase.status: debootstrap/minbase.out
	xargs dpkg --status <$< >$@

debootstrap/minbase.out: | debootstrap debootstrap/target
	{ set -euvx \
	; . /etc/lsb-release \
	; debootstrap \
	    --verbose \
	    --print-debs \
	    --keep-debootstrap-dir \
	    --variant=minbase \
	    "$${DISTRIB_CODENAME}" \
	    $(dir $@)/target \
	    >$@ \
	; }

debootstrap:
	mkdir -vp $@

debootstrap/target:
	mkdir -vp $@

################################################################################

apt-config.mk: $(MAKEFILE_LIST)
	@echo
	@echo Generating: $@
	@apt-config shell \
	APT_ARCHITECTURE "APT::Architecture" \
	APT_ARCHITECTURES "APT::Architectures" \
	DIR_CACHE "Dir::Cache/f" \
	DIR_ETC "Dir::Etc/f" \
	DIR_ETC_MAIN "Dir::Etc::main/f" \
	DIR_ETC_PARTS "Dir::Etc::parts/f" \
	DIR_ETC_SOURCELIST "Dir::Etc::sourcelist/f" \
	DIR_ETC_SOURCEPARTS "Dir::Etc::sourceparts/f" \
	DIR_ETC_TRUSTED "Dir::Etc::trusted/f" \
	DIR_ETC_TRUSTEDPARTS "Dir::Etc::trustedparts/f" \
	DIR_STATE "Dir::State/f" \
	DIR_STATE_STATUS "Dir::State::status/f" \
	| tr -d \' | tee $@

-include apt-config.mk

real_apt_config = \
  $(sort \
    $(DIR_CACHE) \
    $(DIR_ETC_MAIN) \
    $(DIR_ETC_SOURCELIST) \
    $(DIR_ETC_TRUSTED) \
    $(DIR_STATE) \
    $(DIR_STATE_STATUS) \
    $(wildcard \
      $(DIR_ETC_PARTS)/* \
      $(DIR_ETC_SOURCEPARTS)/*.list \
      $(DIR_ETC_TRUSTEDPARTS)/*.asc \
      $(DIR_ETC_TRUSTEDPARTS)/*.gpg \
      ))

fake_apt_config = $(addprefix apt-config, $(real_apt_config))
# $(info fake_apt_config:)
# $(info $(fake_apt_config))

copy_apt_config = $(filter-out \
  apt-config$(DIR_ETC_MAIN) \
  apt-config$(DIR_STATE_STATUS) \
  ,$(fake_apt_config))
# $(info copy_apt_config:)
# $(info $(copy_apt_config))

################################################################################

apt-config$(DIR_ETC_MAIN): $(MAKEFILE_LIST) $(copy_apt_config)
	@echo
	@echo Generating: $@
	@{ true \
	; echo 'APT::Architecture               "$(APT_ARCHITECTURE)";' \
	; echo 'APT::Architectures              "$(APT_ARCHITECTURES)";' \
	; echo 'APT::Get::allow-downgrades      "true";' \
	; echo 'APT::Install-Recommends         "false";' \
	; echo 'APT::Sandbox::User              "$(shell whoami)";' \
	; echo 'APT::Solver                     "aspcud";' \
	; echo 'Acquire::Languages              "none";' \
	; echo 'Acquire::Retries                "3";' \
	; echo 'Debug::NoLocking                "true";' \
	; echo 'Debug::pkgDepCache::AutoInstall "true";' \
	; echo 'Debug::pkgProblemResolver       "true";' \
	; echo 'Dir                             "$(realpath apt-config)";' \
	; echo 'Dir::Cache                      "$(realpath apt-config/$(DIR_CACHE))";' \
	; echo 'Dir::Etc::Preferences           "/dev/null";' \
	; echo 'Dir::Etc::PreferencesParts      "/dev/null";' \
	; echo 'Dir::Etc::parts                 "/dev/null";' \
	; echo 'Dir::Etc::sourcelist            "$(realpath apt-config/$(DIR_ETC_SOURCELIST))";' \
	; echo 'Dir::Etc::sourceparts           "$(realpath apt-config/$(DIR_ETC_SOURCEPARTS))";' \
	; echo 'Dir::Etc::trusted               "$(realpath apt-config/$(DIR_ETC_TRUSTED))";' \
	; echo 'Dir::Etc::trustedparts          "$(realpath apt-config/$(DIR_ETC_TRUSTEDPARTS))";' \
	; echo 'Dir::State                      "$(realpath apt-config/$(DIR_STATE))";' \
	; echo 'Dir::State::status              "$(realpath apt-config/$(DIR_STATE_STATUS))";' \
	; } | tee $@

apt-config$(DIR_STATE_STATUS): $(MAKEFILE_LIST)
	@echo
	@echo Generating: $@
	mkdir -p $(dir $@)
	touch $@

$(copy_apt_config): apt-config/%: /% $(MAKEFILE_LIST)
	@if test -d $< \
	; then install -vd $@ \
	; else install -vD $< $@ \
	; fi

################################################################################

.PHONY: apt-get_update.out
apt-get_update.out: apt-config$(DIR_ETC_MAIN)
	APT_CONFIG=$(realpath $<) apt-get --yes update 2>&1 | tee $@
