#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-pipeline-schedule-run

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    find "${tmpdir}" -type f -exec shred {} +
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... PROJECT_WEB_URL [ TARGET_REF | PIPELINE_SCHEDULE_ID ]
Trigger a new scheduled pipeline.

Options:

    -h    print usage and exit


Examples:

    \$ $0 -h

    \$ $0 https://gitlab.com/realtime-robotics/rapidplan 363745

    \$ $0 https://gitlab.com/realtime-robotics/web/rtr-webapps 1.0.1

Notes:

    * If the second argument is a natural number, then it is assumed to be a
      PIPELINE_SCHEDULE_ID; otherwise it is assumed to be a TARGET_REF.

    * If the second argument is absent, then TARGET_REF defaults to the default
      branch.

EOF
}

curlxxx() {
    curl \
        --silent \
        --retry 3 \
        --fail \
        --show-error \
        --location \
        --header @"${header}" \
        "$@"
}

curlget() {
    info "getting..."
    curlxxx --request GET "$@"
}

curlpost() {
    info "posting..."
    curlxxx --request POST "$@"
}

################################################################################
################################################################################
################################################################################

# Check required commands.
for cmd in curl jq; do
    if ! command -v "${cmd}" >/dev/null 2>&1; then
        die missing command: "${cmd}"
    fi
done

# Discover token and generate header file.
header="$(mktemp -ut header.XXXXXX)"
if [ -n "${GITLAB_TOKEN-}" ]; then
    echo "PRIVATE-TOKEN:" "${GITLAB_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    echo "JOB-TOKEN:" "${CI_JOB_TOKEN}"
elif [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    warning GITLAB_API_PRIVATE_TOKEN is deprecated by GITLAB_TOKEN
    echo "PRIVATE-TOKEN:" "${GITLAB_API_PRIVATE_TOKEN}"
else
    die "need one, missing all:" "GITLAB_TOKEN" "CI_JOB_TOKEN"
fi >"${header}"
readonly header="${header}"

export GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"

################################################################################

# Parse options.
while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

# Vet argument quantity.
case "$#" in
    1 | 2) ;;
    *) die bad args ;;
esac

################################################################################

# Vet the first argument and assign 'PROJECT_WEB_URL'.
if ! PROJECT_WEB_URL="$(echo "$1" | grep -Ex 'https://gitlab[.]com(/[^/]+)+')"; then
    die bad PROJECT_WEB_URL: "$1"
fi
readonly PROJECT_WEB_URL="${PROJECT_WEB_URL}"
info PROJECT_WEB_URL: "${PROJECT_WEB_URL}"

################################################################################

# Assign the 'project_id' using the 'PROJECT_WEB_URL'.
project_id="$(
    echo "${PROJECT_WEB_URL}" \
        | cut -d/ -f4- | tr -d '[:space:]' | jq -sRr @uri
)"
readonly project_id="${project_id}"
info "project_id:" "${project_id}"

################################################################################

# Handle each kind of positional argument quantity.
case "$#" in
    1)
        # The second argument is missing. Query the project 'default_branch'
        # and assign it to 'TARGET_REF'.
        project_url="$(printf '%s/projects/%s' "${GITLAB_API_ENDPOINT}" "${project_id}")"
        readonly project_url="${project_url}"
        info project_url: "${project_url}"
        if ! project="$(curlget "${project_url}")"; then
            die FAILURE: curlget "${project_url}"
        fi
        readonly project="${project}"
        if ! TARGET_REF="$(echo "${project}" | jq -r '.default_branch')"; then
            die FAILURE: jq '.default_branch'
        fi
        readonly TARGET_REF="${TARGET_REF}"
        warning defaulted: TARGET_REF: "${TARGET_REF}"
        ;;
    2)
        # The second argument is present.
        if PIPELINE_SCHEDULE_ID="$(echo "$2" | grep -Ex '[1-9][0-9]+|[0-9]')"; then
            # The second argument is a natural number. Assume that this is the
            # 'PIPELINE_SCHEDULE_ID'.
            true
        else
            # The second argument is NOT a natural number. Assume that this is
            # the 'TARGET_REF'.
            TARGET_REF="$2"
            info TARGET_REF: "${TARGET_REF}"
        fi
        ;;
esac

################################################################################

# Getting to here implies defined/nonempty PIPELINE_SCHEDULE_ID xor
# defined/nonempty TARGET_REF.

if [ -n "${PIPELINE_SCHEDULE_ID-}" ]; then
    info PIPELINE_SCHEDULE_ID: "${PIPELINE_SCHEDULE_ID}"
else
    # https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules
    pipeline_schedules_url="$(
        printf \
            '%s/projects/%s/pipeline_schedules' \
            "${GITLAB_API_ENDPOINT}" \
            "${project_id}"
    )"

    # TODO: Support getting more than 100 pipeline schedules.
    if ! pipeline_schedules="$(
        curlget --data "per_page=100" "${pipeline_schedules_url}"
    )"; then
        die "FAILURE:" curlget --data "per_page=100" "${pipeline_schedules_url}"
    fi

    # Select the pipeline schedules such that .ref == $TARGET_REF.
    pipeline_schedules_ref="$(
        echo "${pipeline_schedules}" \
            | jq --arg ref "${TARGET_REF}" 'map(select(.ref==$ref))'
    )"

    # How many pipeline schedules have .ref == $TARGET_REF?
    case "$(echo "${pipeline_schedules_ref}" | jq 'length')" in
        0)
            die no pipeline schedules for TARGET_REF '"'"${TARGET_REF}"'"'
            ;;
        1)
            PIPELINE_SCHEDULE_ID="$(
                echo "${pipeline_schedules_ref}" | jq '.[0].id'
            )"
            ;;
        *)
            die multiple pipeline schedules for TARGET_REF '"'"${TARGET_REF}"'"': \
                "$(echo "${pipeline_schedules_ref}" | jq -c 'map(.id)')"
            ;;
    esac
    info found: PIPELINE_SCHEDULE_ID: "${PIPELINE_SCHEDULE_ID}"
fi

################################################################################

# https://docs.gitlab.com/ee/api/pipeline_schedules.html#run-a-scheduled-pipeline-immediately
pipeline_schedule_play_url="$(
    printf \
        '%s/projects/%s/pipeline_schedules/%u/play' \
        "${GITLAB_API_ENDPOINT}" \
        "${project_id}" \
        "${PIPELINE_SCHEDULE_ID}"
)"
readonly pipeline_schedule_play_url="${pipeline_schedule_play_url}"
info pipeline_schedule_play_url: "${pipeline_schedule_play_url}"

if ! pipeline_schedule_play="$(curlpost "${pipeline_schedule_play_url}")"; then
    die FAILURE: curlpost "${pipeline_schedule_play_url}"
fi

echo "${pipeline_schedule_play}" | jq '.'

exit "$?"
