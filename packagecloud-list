#!/bin/sh
# shellcheck disable=SC2317
# scrippies/packagecloud-list

set -euvx

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

cleanup() {
    status="$?"
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... REPO
List the packages in the given REPO.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 realtime-robotics/incoming

EOF
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if ! [ 1 -eq "$#" ]; then
    die "bad args"
fi

packages_url="$(
    printf '/api/v1/repos/%s/packages.json' "$1"
)"
info packages_url: "${packages_url}"
packages_out="$(
    echo "${packages_url}" \
        | cut -d/ -f5- \
        | sed -E 's/[^[:alnum:].-]+/_/g;s/^_//;s/_$//'
)"
info packages_out: "${packages_out}"

"${here}"/packagecloud-curl GET "${packages_url}" >"${packages_out}"
jq '.' "${packages_out}" >"${packages_out}".pretty
mv -v "${packages_out}".pretty "${packages_out}" >&2

exit "$?"
