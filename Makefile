# scrippies/Makefile

prefix ?= /usr/local
bindir = $(prefix)/bin

shell_scripts := \
  apt-install-build-deps \
  apt-install-run-deps \
  apt-omg-wtf \
  aws-policy-template-and-push \
  batch-create-s3-buckets \
  branch-rebasify \
  bump-ros-ws \
  cherry_blame.sh \
  configure-apt \
  copyright-licenses \
  core-dump-self \
  crongen \
  deb-licenses \
  delete-kernel \
  docker-packagecloud \
  dpkg-buildpackage-dput \
  dpkg-licenses \
  gbp-dch-release \
  generate-cmake.sh \
  gh-actions-token-repo \
  git-build-recipe-sbuild \
  git-remote-subtract \
  git-submodule-update-message \
  git-topiary \
  git2origtar \
  gitlab-get-info \
  gitlab-group-projects \
  gitlab-group-vars \
  gitlab-integration-pipeline-email \
  gitlab-job \
  gitlab-job-artifacts \
  gitlab-job-log \
  gitlab-member-invite \
  gitlab-member-remove \
  gitlab-pipeline \
  gitlab-pipeline-create \
  gitlab-pipeline-schedule \
  gitlab-pipeline-schedule-run \
  gitlab-pipeline-schedules \
  gitlab-pipeline-test-report \
  gitlab-project-invite \
  gitlab-project-vars \
  gitlab-repository-archive \
  gitlab-trigger-pipeline \
  gitlab-zipballer \
  gostatic \
  install-deb \
  install-docker-compose \
  install-golang \
  jira-attach \
  jira-close \
  jira-comment-errout \
  jira-issue-create \
  jira-kn \
  kvm-img \
  kvm-usb \
  lawyer-chow \
  lp-dput \
  mk-run-deps \
  mkrtlinux \
  mount-sshfs \
  new-developer-invite \
  p5000 \
  packagecloud-apt \
  packagecloud-curl \
  packagecloud-destroy \
  packagecloud-email \
  packagecloud-list \
  packagecloud-promote \
  push-all \
  release-it \
  remkernel \
  ros-gensymbols \
  rtr-vpn-up \
  ssh-keygen-copy \
  system-report \
  tar-deterministic \
  twiddle-pubkeys \
  video-recode.sh \
  wrap-catkin \
  wrap-colcon

awk_scripts := \
  gsuite-users-mutt-aliases.awk

yml_files := \
  .gitlab-ci.yml

.PHONY: all
all:
	echo $@: noop

.PHONY: check
check: shellcheck shfmt yamllint
	./test/install-deb
	./test/apt-install-build-deps

# https://www.shellcheck.net/wiki/SC2317 : Command appears to be
# unreachable. Check usage (or ignore if invoked indirectly).
#
# https://www.shellcheck.net/wiki/SC2320 : This $? refers to echo/printf, not a
# previous command. Assign to variable to avoid it being overwritten.
#
.PHONY: shellcheck
shellcheck: $(shell_scripts)
	shellcheck \
		--external-sources \
		--exclude=SC2317 \
		--exclude=SC2320 \
		$^

.PHONY: shfmt
shfmt: $(shell_scripts)
	shfmt -i 4 -ci -bn -s -d -w -ln posix $^

.PHONY: gawklint
gawklint: $(awk_scripts)
	for awk_script in $^; do gawk --lint --file="$${awk_script}" ; done </dev/null

.PHONY: yamllint
yamllint: $(yml_files)
	yamllint $^

.PHONY: install
install:
	mkdir -vp $(DESTDIR)$(bindir)
	cp -va \
		apt-install-build-deps \
		install-deb \
		$(DESTDIR)$(bindir)

.PHONY: clean
clean:
	echo $@: noop
