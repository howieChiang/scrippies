#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-member-invite

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    find "${tmpdir}" -type f -exec shred {} +
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... TARGET EMAIL ACCESS_LEVEL
Add a member to a group or project.

Options:

    -h    print usage and exit
    -n    noop, don't actually do anything

Examples:

    \$ $0 -h

    \$ $0 realtime-robotics luser@example.com 30

    \$ $0 realtime-robotics/rapidplan luser@example.com 40

Notes:

    https://docs.gitlab.com/ee/api/invitations.html#valid-access-levels

    Valid access levels

    To send an invitation, you must have access to the project or group you are
    sending email for. Valid access levels are defined in the Gitlab::Access
    module. Currently, these levels are valid:

    No access (0)
    Minimal access (5) (Introduced in GitLab 13.5.)
    Guest (10)
    Reporter (20)
    Developer (30)
    Maintainer (40)
    Owner (50) - Only valid to set for groups

EOF
}

noisy_eval() (
    info "RUNNING:" "$@"
    if "$@"; then
        rc="$?"
        info "SUCCESS:" "$@"
    else
        rc="$?"
        error "FAILURE:" "$@"
    fi
    return "${rc}"
)

maybe_noop() (
    if [ "true" = "${requested_noop:-false}" ]; then
        info "DRY RUN:" "$@"
    else
        info "WET RUN:" "$@"
        noisy_eval "$@"
    fi
)

curlxxx() {
    curl \
        --fail \
        --header @"${header}" \
        --location \
        --retry 3 \
        --show-error \
        --silent \
        "$@"
}

curlget() {
    info "getting..."
    curlxxx --request GET "$@"
}

curlpost() {
    info "posting..."
    curlxxx --request POST "$@"
}

################################################################################
################################################################################
################################################################################

for cmd in curl jq; do
    if ! command -v "${cmd}" >/dev/null 2>&1; then
        die missing command: "${cmd}"
    fi
done

while getopts ":hn" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        n) requested_noop="true" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if ! [ 3 -eq "$#" ]; then
    die "bad args"
fi

################################################################################

TARGET="$1"
EMAIL="$2"
ACCESS_LEVEL="$3"
info TARGET: "${TARGET}"
info EMAIL: "${EMAIL}"
info ACCESS_LEVEL: "${ACCESS_LEVEL}"

################################################################################

export GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"
info "GITLAB_API_ENDPOINT:" "${GITLAB_API_ENDPOINT}"

################################################################################

# Discover token and generate header file.
header="$(mktemp -ut header.XXXXXX)"
if [ -n "${GITLAB_TOKEN-}" ]; then
    echo "PRIVATE-TOKEN:" "${GITLAB_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    echo "JOB-TOKEN:" "${CI_JOB_TOKEN}"
elif [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    warning GITLAB_API_PRIVATE_TOKEN is deprecated by GITLAB_TOKEN
    echo "PRIVATE-TOKEN:" "${GITLAB_API_PRIVATE_TOKEN}"
else
    die "need one, missing all:" "GITLAB_TOKEN" "CI_JOB_TOKEN"
fi >"${header}"

################################################################################

# Determine if the TARGET is a group or a project.
target_id="$(printf '%s' "${TARGET}" | jq -sRr @uri)"
info target_id: "${target_id}"

# https://docs.gitlab.com/ee/api/invitations.html#add-a-member-to-a-group-or-project
group_invitations_url="${GITLAB_API_ENDPOINT}"/groups/"${target_id}"/invitations
project_invitations_url="${GITLAB_API_ENDPOINT}"/projects/"${target_id}"/invitations
if curlget "${group_invitations_url}" >/dev/null 2>&1; then
    info found group: "${TARGET}"
    target_invitations_url="${group_invitations_url}"
elif curlget "${project_invitations_url}" >/dev/null 2>&1; then
    info found project: "${TARGET}"
    target_invitations_url="${project_invitations_url}"
else
    error missing: group '"'"${TARGET}"'"'
    error missing: project '"'"${TARGET}"'"'
    die neither a group nor a project: "${TARGET}"
fi
info target_invitations_url: "${target_invitations_url}"

################################################################################

output="$(mktemp -ut output.XXXXXX)"
maybe_noop curlpost \
    --data-urlencode "email=${EMAIL}" \
    --data-urlencode "access_level=${ACCESS_LEVEL}" \
    --data-urlencode "invite_source=${EMAIL}" \
    --output "${output}" \
    "${target_invitations_url}"

if ! [ "true" = "${requested_noop:-false}" ]; then
    info output: "$(jq '.' "${output}")"
fi

exit "$?"
