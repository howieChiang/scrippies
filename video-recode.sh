#!/bin/sh
# shellcheck disable=SC2317
# scrippies/video-recode.sh

set -euvx

# Explanations:
#
# '-vcodec libx265' because it is empirically pretty good
#
# '-acodec libopus' because it is empirically pretty good
#
# '-scodec mov_text' because it is supported by the mp4 container.
#
# '-strict -2' because
# https://stackoverflow.com/questions/54192916/error-opus-in-mp4-support-is-experimental-add-strict-2-if-you-want-to-use
#
ffmpeg \
    -i "$1" \
    -vcodec libx265 \
    -acodec libopus \
    -scodec mov_text \
    -strict -2 \
    "$(printf '%s.new.mp4' "${1%.*}" | tr -sc '/[:alnum:].-' '_')"
